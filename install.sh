#!/bin/bash

# Set this parameter to the number of cores that you would like to use!
num_cores=8

# Getting ConceptNet version 5.5
cd conceptnet
wget https://s3.amazonaws.com/conceptnet/precomputed-data/2016/assertions/conceptnet-assertions-5.5.0.csv.gz
gzip -d conceptnet-assertions-5.5.0.csv.gz
python preprocess.py
cd ..

# Getting ESPGAMES
cd espgame
wget http://www.cs.toronto.edu/~rntoro/resources/ESPGame100k.tar.gz
tar -xvzf ESPGame100k.tar.gz
python preprocess.py
cd ..
    
# Getting MS-COCO validation set captions
cd mscoco
wget http://msvocds.blob.core.windows.net/annotations-1-0-3/captions_train-val2014.zip
unzip captions_train-val2014.zip
python preprocess.py --num_cores=$num_cores
cd ..

# Getting precomputed detections
cd detectors
wget http://www.cs.toronto.edu/~rntoro/resources/detections.zip
unzip detections.zip
cd ..