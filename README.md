# CN-Detectors

## About this project

This project studies how general-purpose ontologies, such as MIT's ConceptNet ontology, can improve the performance of state-of-the-art vision systems. As a first step, we tackle the problem of sentence-based image retrieval. A description of our approach can be found in the following paper:

    @inproceedings{toro2017ontology,
      title={How a General-Purpose Commonsense Ontology can Improve Performance of Learning-Based Image Retrieval},
      author={Toro Icarte, Rodrigo and Baier, Jorge A. and Ruz, Cristian and Soto, Alvaro},
      booktitle={26th International Joint Conference on Artificial Intelligence},
      year={2017}
    }

This code is a clean and updated version of the code that we used in our paper. 
The main differences are that it uses the latest version of ConceptNet and a different POS tagger and word stemming algorithm.
As a result, the performance is slightly better than the one reported in the paper. In addition, we included 
a new score that outperforms CN-MAX :)

## Installation instructions

### Prerequisites

This code uses [Python2.7](https://www.python.org/download/releases/2.7/) with three libraries: [numpy](http://www.numpy.org/), [nltk](http://www.nltk.org/), and [enum34](https://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python). After installing nltk, it might be necessary to download the maxent_treebank_pos_tagger and punkt packages:

    import nltk
    nltk.download() # Select 'maxent_treebank_pos_tagger' from the list of models and 'punkt' from the list of packages.

### Cloning this repository and getting external resources

You may clone this repository by running:

    git clone https://bitbucket.org:RToroIcarte/cn-detectors.git
    cd cn-detectors
    
Then, you may run the "install.sh" script to finish the installation. However, we encourage you to update the number of cores to use (install.sh -> line 4) before running it:

    ./install.sh

Now you just have to wait (... several hours). This script downloads ConceptNet5.5, ESPGAMES, the MSCOCO annotations, and the detections.json file. To save space on your disk, it does not download the MSCOCO images. To save time in your life, we precomputed the word detections for the validation set and summarized the results in the detections.json file. If you want to train and rerun the detectors, you will have to download [MSCOCO](http://mscoco.org/dataset/#download) and install the [visual-concept detectors](https://github.com/s-gupta/visual-concepts). This script also preprocesses ConceptNet, ESPGAMES, and MSCOCO. These preprocessing steps speed up future computations. 

### Running examples

Finally, you may run some examples using the "image_retrieval.py" code (which receives 3 parameters: the number of cores to use, the path to the test set file, and the path to the score configuration file). A few examples follow:

    python image_retrieval.py --num_cores=8 --test_file="./tests/coco-1k.txt" --score="./config/mil.json"
    python image_retrieval.py --num_cores=8 --test_file="./tests/coco-1k.txt" --score="./config/milstem.json"
    python image_retrieval.py --num_cores=8 --test_file="./tests/coco-1k.txt" --score="./config/espgame_max.json"
    python image_retrieval.py --num_cores=8 --test_file="./tests/coco-1k.txt" --score="./config/cn_max.json"
    python image_retrieval.py --num_cores=8 --test_file="./tests/coco-1k.txt" --score="./config/cn_gwa.json"

