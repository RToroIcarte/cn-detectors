import json

def is_ascii(s):
	try:
		#s.decode('utf-8')
		s.decode('ascii')
		return True
	except UnicodeError:
		print "Warning! I'm ignoring this string because it is not in ascii:", s
		return False

def read_json(path):
	with open(path) as data_file:    
		json_in = json.load(data_file)
	return json_in

def save_json(path, data):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)