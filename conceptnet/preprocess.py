"""
This code "cleans" the original CN file extracting only the information that we use in our experiments.
In particular:
    - It only keeps assertions between concepts such that:
        1) Both are English concepts
        2) Both are single words of length greater than 1
    - It exports relations in the following format: "\t".join([c1,relation,c2,weight])"
"""

import os, json, sys
sys.path.insert(0, '../util')
from util import is_ascii

def preprocess_conceptnet():
    print "Preprocessing conceptnet..."
    file = "./conceptnet-assertions-5.5.0.csv"
    if not os.path.isfile(file):
        print "Error! I couldn't find " + file
        exit()
    # Create new folder
    folder_out = "./CN"
    if not os.path.exists(folder_out):
        os.makedirs(folder_out)
    # Reading and Exporting CN
    f_in = open(file)
    f_out = open(os.path.join(folder_out, "conceptnet.csv"),'w')
    for l in f_in:
        uri, relation, node_start, node_end, others = tuple(l.rstrip().split('\t'))
        if check_node(node_start) and check_node(node_end):
            # Extracting single word
            node_start, node_end = get_word(node_start), get_word(node_end)
            if len(node_start) > 1 and len(node_end)> 1:
                # extracting the weight
                weight = json.loads(others)["weight"]
                f_out.write("\t".join([node_start,relation,node_end,str(weight)]) + "\n")
    # Closing files
    f_out.close()
    f_in.close()

def check_node(e):
    return e.startswith('/c/en/') and "_" not in e and is_ascii(e)

def get_word(w):
    return w.replace('/c/en/','').split('/')[0]



if __name__ == "__main__":
    preprocess_conceptnet()