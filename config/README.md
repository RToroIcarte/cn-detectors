# Score Configuration Files

Each json file contains all the configuration parameters needed to define one score function. An example follows:

    {
        # This is the score type. 
        # The options are: "ScoreType.mil", "ScoreType.espgame", and "ScoreType.conceptnet"
        "score_type": "ScoreType.conceptnet", 

        # This defines the aggregation function to use. 
        # The options are: "Aggregation.min", "Aggregation.mean_a", "Aggregation.mean_g", and "Aggregation.max"
        "f_aggregation": "Aggregation.max",
        
        # This defines if the score uses word stemming.
        # The options are: "False" and "True"
        "use_stemming": "True",
        
        # This defines the types of detectors that the score uses
        # Its value must be a subset of: ["NN","VB","JJ","other"]
        "detector_filter": ["NN","VB","JJ"],
        
        # This defines the types of words that the score tries to detect using CN or ESPGAMES
        # Its value must be a subset of: ["NN","VB","JJ","other"]
        "new_word_filter": ["NN","VB","JJ"]
    }