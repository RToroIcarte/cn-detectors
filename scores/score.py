"""
This abstract class defines an interface to create new scores.
"""

class Score(object):
    """
    This method is used by image_retrieval.py to rank each image.
    It must return a float that represents how well the caption (words) given the word detections (word_detections)
    """
    def get_score(self, words, word_detections):
        raise NotImplementedError('subclasses must override get_score(self, image_id, words)!')
