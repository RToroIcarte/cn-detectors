"""
This method implements the MIL score described on the paper.
It is just a multiplication of the word's probabilities
"""

import sys, math
from mil import MilScore
from settings import Aggregation, ScoreMethod
from util.util import read_json

class EspgameScore(MilScore):
	"""
	parameters:
		- "detector_filters" is a list that should include at least one of the following elements: "NN", "VB", "JJ", "other".
		  They define which types of detectors we will use.
		- stem is a boolean. True means that we will stem words to increase the number of detectable words
		- "word_filters" is a list of POS that we will consider for zero-shot learning ("NN", "VB", "JJ", "other")
		- "annotations" is a list with all the captions that we will use (this allows us to precompute the espgame probabilities)
		- "score_method" is the score method to use (total probabilities or geometric weighted average)
		- "f_aggregation" is a flag indicating the aggregation function
	"""
	def __init__(self, detector_filters, stem, word_filters, annotations, score_method, f_aggregation):
		# Loading detectors (self.detectors)
		MilScore.__init__(self, detector_filters, stem)
		# Loading espgames
		self.word_filters = word_filters
		self.unknown_words = self.get_unknown_words(annotations) # this is the set of words for which we don't have a detector
		self.load_espagames()
		self.score_method = score_method
		self.f_aggregation = f_aggregation
		
	"""
	It returns the multiplication of the word probabilities
	"""
	def get_score(self, words, word_detections):
		# getting raw mil score
		base_score = MilScore.get_score(self, words, word_detections)

		# getting undetected words that we can detect using espgames/conceptnet
		cn_detectable_words = set()
		for pos in self.word_filters:
			for w in words[pos]:
				w = self.normalize(w)
				if w in self.unknown_words:
					cn_detectable_words.add(w)

		# Assigning probabilities to each new detector
		for w in cn_detectable_words:
			if self.score_method == ScoreMethod.total_probability:
				base_score *= self.score_total_probabilities(w, word_detections)
			if self.score_method == ScoreMethod.geometric_mean:
				base_score *= self.score_geometric_mean(w, word_detections)

		return base_score

	"""
	Returns an estimation of the probability that 'w' using the total probability rule.
	This method was described in the paper
	"""
	def score_total_probabilities(self, w, word_detections):
		# computing probability for this undetectable word
		scores = []
		probs = self.unknown_probs[w]
		for d in probs:
			w0 = probs[d].get_prob_y_given_not_x()
			w1 = probs[d].get_prob_y_given_x()
			if max([w0, w1]) > 0: # if not, then 'w' or 'd' don't appear in espgames
				# detector probability
				if self.stem: P = self.get_stemmed_detector_probability(word_detections, d)
				else:         P = word_detections[d] 
				# total probability-like score
				scores.append(P*w1 + (1-P)*w0)
		# aggregating estimations
		prob_w = 1
		if scores:
			if self.f_aggregation == Aggregation.min:     # minimum
				prob_w = min(scores)
			if self.f_aggregation == Aggregation.mean_a:  # arithmetic mean
				prob_w = sum(scores)/len(scores)
			if self.f_aggregation == Aggregation.mean_g:  # geometric mean
				for i in range(len(scores)):
					scores[i] = math.log(scores[i])
				prob_w = math.exp(sum(scores)/len(scores))
			if self.f_aggregation == Aggregation.max:     # maximum
				prob_w = max(scores)
		return prob_w
		
	"""
	Returns an estimation of the probability that 'w' using a gemetric weighted average.
	We discovered this method after publication. It is competitive with CN_MAX and somehow simpler.
	"""
	def score_geometric_mean(self, w, word_detections):
		score, weight = 0,0
		probs = self.unknown_probs[w]
		for d in probs:
			# P(w|d)
			w = probs[d].get_prob_y_given_x()
			# P(d|I)
			if self.stem: P = self.get_stemmed_detector_probability(word_detections, d)
			else:         P = word_detections[d] 
			# Updating score
			score += math.log(P)*w
			weight += w
		if weight > 0:
			return math.exp(score/weight)
		return 1

	"""
	Returns a matrix unknown_probs such that:
		- unknown_probs[i][j] contains P(i|j) and P(i|not j), where 
			- 'i' is an undetectable word (self.unknown_words)
			- 'j' is a detectable one (self.detectors)
	* In this case, we assume that each unknown_words is connected to every 'known word'.
	  To consider CN, we overwrite this method to only connect words that are at distance 1 in conceptnet
	"""
	def create_probability_array(self):
		unknown_probs, total_Y, total_X = {},{},{}
		detectors = self.stemmed_detectors if self.stem else self.detectors
		for w in self.unknown_words:
			unknown_probs[w] = {}
			total_Y[w] = 0
			for d in detectors:
				unknown_probs[w][d] = Probability()				
				if d not in total_X:
					total_X[d] = 0
		return unknown_probs, total_Y, total_X
		
	"""
	This method precomputes the espgame probabilities that we will need during image retrieval
	"""
	def load_espagames(self):
		print "Loading espgames...",
		sys.stdout.flush()
		
		# Creating auxiliary arrays for keeping the probabilities: P(y|x)
		self.unknown_probs, total_Y, total_X = self.create_probability_array()

		# Computing the espgame probabilities: P(y|x), where 'y' is an unknown word, but we have a detector for 'x'
		path = './espgame/data/espgame.json'
		espgame = read_json(path)
		for captions in espgame:
			# normalizing captions (i.e. stemming them if self.stem is True)
			captions = set([self.normalize(c) for c in captions])
			# Counting captions
			for c1 in captions:
				if c1 in total_X: 
					total_X[c1] += 1
				if c1 in total_Y:
					total_Y[c1] += 1
					for c2 in captions:
						if c2 in self.unknown_probs[c1]:
							self.unknown_probs[c1][c2].ok_1 += 1
		
		# Generating final results
		total = len(espgame)
		for y in self.unknown_probs:
			for x in self.unknown_probs[y]:
				self.unknown_probs[y][x].total_1 = total_X[x]
				self.unknown_probs[y][x].total_0 = total - total_X[x]
				self.unknown_probs[y][x].ok_0 = total_Y[y]-self.unknown_probs[y][x].ok_1
		
		print "done!"

	"""
	Returns a set of words for which we don't have a detector
	"""
	def get_unknown_words(self, annotations):
		unknown_words = set()
		for a in annotations:
			for pos in self.word_filters:
				for w in a['words'][pos]:
					if self.is_unknown_word(w):
						unknown_words.add(self.normalize(w))
		return unknown_words

	"""
	Returns True if there is no detector for 'w'
	"""
	def is_unknown_word(self, w):
		mil_detectable = w in self.detectors
		if self.stem:
			milstem_detectable = mil_detectable or self.get_stemming(w) in self.stemmed_detectors
			return not milstem_detectable
		return not mil_detectable

"""
This is an auxiliary class to keep track of the probabilities
"""
class Probability:
	def __init__(self): 
		# For P(Y|not X)
		self.ok_0 = 0
		self.total_0 = 0

		# For P(Y|X)
		self.ok_1 = 0
		self.total_1 = 0

	# returns P(Y|X)
	def get_prob_y_given_x(self):
		return self.get_prob(self.ok_1, self.total_1)
	
	# returns P(Y|not X)
	def get_prob_y_given_not_x(self):
		return self.get_prob(self.ok_0, self.total_0)

	def get_prob(self, ok, total):
		if total > 0:
			return ok/float(total)
		return 0 #0.01
		