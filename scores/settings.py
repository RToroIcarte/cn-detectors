"""
This code contains several enum's with options to configurate the scores.
"""
from enum import Enum

"""
Auxiliary enum to define the score to use
"""
class ScoreType(Enum):
    mil        = 1
    espgame    = 2
    conceptnet = 3

"""
We explored two methods to approximate the probability for a new word.
"""
class ScoreMethod(Enum):
    total_probability = 1
    geometric_mean    = 2

"""
The CN-Score aggregates estimations of P(w) using one of the following aggregation functions.
"""
class Aggregation(Enum):
    min    = 1 # minimum
    mean_a = 2 # arithmetic mean
    mean_g = 3 # geometric mean
    max    = 4 # maximum
