"""
This method implements the MIL score described on the paper.
It is just a multiplication of the word's probabilities
"""

from espgames import EspgameScore, Probability

class ConceptnetScore(EspgameScore):
	"""
	parameters:
		- "detector_filters" is a list that should include at least one of the following elements: "NN", "VB", "JJ", "other".
		  They define which types of detectors we will use.
        - stem is a boolean. True means that we will stem words to increase the number of detectable words
		- "word_filters" is a list of POS that we will consider for zero-shot learning ("NN", "VB", "JJ", "other")
		- "score_method" is the score method to use (total probabilities or geometric weighted average)
		- "annotations" is a list with all the captions that we will use (this allows us to precompute the espgame probabilities)
	"""
	def __init__(self, detector_filters, stem, word_filters, annotations, score_method, f_aggregation):
		EspgameScore.__init__(self, detector_filters, stem, word_filters, annotations, score_method, f_aggregation)

	"""
	Returns a matrix unknown_probs such that:
		- unknown_probs[i][j] contains P(i|j) and P(i|not j), where 
			- 'i' is an undetectable word (self.unknown_words)
			- 'j' is a detectable one (self.detectors)
			- there exists a link between 'i' and 'j' in CN
	"""
	def create_probability_array(self):
		# First, we have to load conceptnet
		self.load_conceptnet()
		# Now we can create the matrix
		unknown_probs, total_Y, total_X = {},{},{}
		for w in self.cn:
			unknown_probs[w] = {}
			total_Y[w] = 0
			for d in self.cn[w]:
				unknown_probs[w][d] = Probability()				
				if d not in total_X:
					total_X[d] = 0
		return unknown_probs, total_Y, total_X
	
	"""
	Loads the subset of conceptnet that we could use given the detectors that we have...
		- cn[w] contains the list of detectors that we could use to detect 'w'
	"""
	def load_conceptnet(self):
		print "Loading conceptnet..."
		self.cn = {}
		# if self.stem is True, I use the normalized version of the detectors to build coneptnet
		detectors = self.stemmed_detectors if self.stem else self.detectors
		file = "./conceptnet/CN/conceptnet.csv"
		f = open(file)
		for l in f:
			c1,r,c2,weight = tuple(l.rstrip().split('\t'))
			if float(weight) >= 1:
				c1 = self.normalize(c1)
				c2 = self.normalize(c2)
				w, d = None, None
				if c1 in self.unknown_words and c2 in detectors: w, d = c1, c2
				if c2 in self.unknown_words and c1 in detectors: w, d = c2, c1
				if w is not None:
					if w not in self.cn:
						self.cn[w] = set()
					self.cn[w].add(d)
		f.close()
		# updating the list of unknown words that we can detect
		self.unknown_words = set(self.cn.keys())
		

		
