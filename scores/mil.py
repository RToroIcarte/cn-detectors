"""
This method implements the MIL score described on the paper.
It is just a multiplication of the word's probabilities
"""

from score import Score
from nltk.stem import SnowballStemmer

class MilScore(Score):
	"""
	parameters:
		- detector_filters is a list that should include at least one of the following elements: "NN", "VB", "JJ", "other".
		  They define which types of detectors we will use.
		- stem is a boolean. True means that we will stem words to increase the number of detectable words
	"""
	def __init__(self, detector_filters, stem):
		# Loading detectors indicated by the filter
		if "other" in detector_filters:
			detector_filters.extend(['DT','PRP','IN'])
		self.detectors = set()
		f = open("./detectors/vocab_words.txt")
		for l in f:
			r = l.rstrip().split(",")
			if r[1].strip() in detector_filters:
				self.detectors.add(r[0].strip())
		f.close()
		# Creating stemmer (if needed)
		self.stem = stem
		if stem: 
			# I remember the known stems to speed up computation
			self.known_stem = {}
			# Creating stemmed version of the detectors
			self.stemmer = SnowballStemmer("english")
			self.stemmed_detectors = {}
			for d in self.detectors:
				sd = self.get_stemming(d)
				if sd not in self.stemmed_detectors: 
					self.stemmed_detectors[sd] = []
				self.stemmed_detectors[sd].append(d)

	"""
	It returns the multiplication of the word probabilities
	"""
	def get_score(self, words, word_detections):
		# getting detectable words
		detectable_words = set()
		undetectable_words = set()
		for t in words:
			for w in words[t]:
				if w in self.detectors: detectable_words.add(w)
				elif self.stem:         undetectable_words.add(self.get_stemming(w))
		# Computing MIL score
		s = 1
		for w in detectable_words:
			s *= word_detections[w]
		# Adding MILSTEM detections (if needed)
		if self.stem:
			for w in undetectable_words:
				if w in self.stemmed_detectors:
					s*= self.get_stemmed_detector_probability(word_detections, w)

		return s

	"""
	stems the word 'w'
	I keep a mapping from words to their stemmed version to speed up computations
	"""
	def get_stemming(self, w):
		if w not in self.known_stem:
			self.known_stem[w] = self.stemmer.stem(w)
		return self.known_stem[w]

	"""
	returns the detection probability of word 'w'
		- if stem is True, we return the maximum between the detector candidates
	"""
	def get_stemmed_detector_probability(self, word_detections, w):
		return max([word_detections[d] for d in self.stemmed_detectors[w]])

	"""
	returns the version of the word 'w' that we should use depending on the value of self.stem
	"""
	def normalize(self, w):
		if self.stem:
			return self.get_stemming(w)
		return w