"""
This code adds POSTagger to each word in the MSCOCO val-set annotations. 
The tags are computed using the StanfordPOSTagger

JSON format example:
{
	"16977": # -> this is the image id 
	[
		{
			"id": 89,  # -> this is the annotation id 
			"caption": "A car that seems to be parked illegally behind a legally parked car", # -> this is the caption
			"image_id": 203564,  # -> this is the image id 
			"words":   # -> this are the tagged words
			{  
				"VB": ["seems", "be", "parked"], 
				"other": ["A", "that", "to", "illegally", "behind", "a", "legally"], 
				"JJ": [], 
				"NN": ["car"]
			}
		},
		{...} # -> In general, each image has 5 annotations
	], 
	"106140": [...] # -> another image...
} 
"""
import os, sys, nltk, argparse
sys.path.insert(0, '../util')
from util import *
from multiprocessing import Process

class myThread(Process):
	def __init__(self, json_in, file_out, init, end):
		Process.__init__(self)
		self.init = init
		self.end = end
		self.json_in = json_in
		self.file_out = file_out

	def run(self):
		# Exporting a json with only the annotations and POS of each word
		json_out = {}
		for i in range(self.init,self.end):
			if (i - self.init)%100 == 0 and i != self.init:
				print str(self.end - i) + "\t -> " + str(i) + "/" + str(self.end)
			ann = self.json_in["annotations"][i]
			img_id = ann["image_id"]
			ann["words"] = tag_words(normalize(ann["caption"]))
			if img_id not in json_out:
				json_out[img_id] = []
			json_out[img_id].append(ann)
		save_json(self.file_out, json_out)


"""
Returns a dictionary with the words classified by their type
"""
def tag_words(sentence):
	is_sentence_ascii = is_ascii(sentence)
	ret = {}
	ret["NN"],ret["VB"],ret["JJ"],ret["other"] = set(),set(),set(),set()
	words = nltk.pos_tag(nltk.tokenize.word_tokenize(sentence))
	for w,pos in words:
		prefix = pos[:2]
		if prefix not in ret:
			prefix = "other"
		if is_sentence_ascii or is_ascii(w):
			ret[prefix].add(w)
	for t in ["NN", "VB", "JJ", "other"]:
		ret[t] = list(ret[t])
	return ret

"""
Returns a normalized version of the caption 's' (following NeuralTalk rules)
"""
def normalize(s):
	s = s.replace("'","")
	s = s.replace(".","")
	s = s.lower()
	chars = '?!,.\'"\n\t-_/;@$#+()[]`*>%'
	for c in chars:
		s = s.replace(c,' ')
	while('  ' in s):
		s = s.replace('  ',' ')
	if(s.endswith(' ')):
		s = s[:len(s)-1]
	if(s.startswith(' ')):
		s = s[1:]
	return s

def preprocess_annotations(num_cores):
	print "Preprocessing validation set annotations..."
	file = "./annotations/captions_val2014.json"
	if not os.path.isfile(file):
		print "Error! I couldn't find " + file
		exit()

	# Create new folder
	folder_out = "./tagged_annotations"
	file_out = os.path.join(folder_out, "captions_val2014_with_tags_norm.json")
	if not os.path.exists(folder_out):
		os.makedirs(folder_out)

	# Reading the annotations 
	json_in = read_json(file)

	# Dividing the annoptations in several threads
	th = []
	total_ann = len(json_in["annotations"])
	delta = total_ann//num_cores + (1 if total_ann%num_cores != 0 else 0)
	for i in range(num_cores):
		init, end = delta*i, min([delta*(i+1),total_ann])
		print str(init) + " -> " + str(end) 
		p = myThread(json_in, file_out + str(i), init, end)
		p.start()
		th.append(p)

	# Computing final json
	json_out = {}
	for i in range(len(th)):
		th[i].join()
		file_i = file_out + str(i)
		json_i = read_json(file_i)
		for img in json_i:
			if img not in json_out:
				json_out[img] = []
			json_out[img].extend(json_i[img])
		os.remove(file_i)

	# Saving final json
	save_json(file_out, json_out)

if __name__ == "__main__":
	# Getting params	
	parser = argparse.ArgumentParser()
	parser.add_argument('--num_cores', default=8, type=int, 
						help='It is the number of cores in which you want to split the computation.')
	args = parser.parse_args()
	preprocess_annotations(args.num_cores)