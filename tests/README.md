It contains the subsets of the validation set that we used for image retrieval.

The subsets 1K and 5K are the same that Karpathy and Fei-Fei (2015) used.

[Karpathy and Fei-Fei, 2015] A.  Karpathy  and  L.  Fei-Fei.Deep visual-semantic alignments for generating image de-scriptions. InCVPR, pages 3128–3137, 2015.
