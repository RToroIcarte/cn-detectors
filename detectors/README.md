# Visual Concepts

We computed a .json file with the detections for each image in the validation set using the trained model that is available in https://github.com/s-gupta/visual-concepts

This is an example of the json file format (detections.json):

    {
        "356787": # -> Image id
        {
            "shop": 0.0063870549201965332,   # Probability of 'shop' given the image 356787
            "all": 0.035810649394989014,     # Probability of 'all' given the image 356787
            "seated": 0.0022474527359008789, # Probability of 'seated' given the image 356787
            ...,                             # Other detectors
            "having": 0.0020260214805603027  # Probability of 'having' given the image 356787
        }, 
        "560222":
        {
            "shop": 0.0021542906761169434, 
            "all": 0.061873972415924072, 
            "seated": 0.00045812129974365234, 
            ...,
            "having": 0.0030661225318908691
        },
        ...   # -> more images
    }

The file 'vocab_words.txt' contains a list with all the available detectors and their POS tags