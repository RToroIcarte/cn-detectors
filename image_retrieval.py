import os, sys, argparse
import numpy as np
from multiprocessing import Process, Manager
from scores.mil import MilScore
from scores.espgames import EspgameScore
from scores.conceptnet import ConceptnetScore
from scores.settings import *
from util.util import *

"""
TODO: Comment this...
"""

class myThread(Process):
	def __init__(self, score, images, annotations, detections, init, end, result):
		Process.__init__(self)
		
		# Attributes
		self.init = init
		self.end = end
		self.score = score
		self.images = images
		self.annotations = annotations
		self.detections = detections

		# Results
		self.result = result
	
	def tuple_key(self, e):
		return e[1]

	def run(self):
		# For every image
		for i in range(self.init,self.end):
			if((i - self.init)%100 == 0 and i != self.init):
				print str(self.end - i) + "\t -> " + str(i) + "/" + str(self.end)
			
			target = str(self.annotations[i]["image_id"])
			words = self.annotations[i]["words"]
			
			# Computing image retrieval
			scores = [(img, self.score.get_score(words, self.detections[img])) for img in self.images]
			scores = sorted(scores,key=self.tuple_key,reverse=True)
			for i in range(len(scores)):
				if(scores[i][0] == target):
					# We consider worse case when there are ties
					j = i
					while(j < len(scores)-1 and scores[i][1] == scores[j+1][1]): 
						j += 1
					self.result[j] += 1
					break

def get_preprocess_file(test, file):
	file_name = "_" + os.path.basename(os.path.splitext(test)[0]) + ".json"
	return os.path.join("./tmp", os.path.basename(file).replace(".json", file_name))

def read_annotations(test_file, annotation_file, images):
	# If we already computed this file, we just read it
	preprocess_file = get_preprocess_file(test_file, annotation_file)
	if os.path.exists(preprocess_file):
		return read_json(preprocess_file)
	# We generate the file from scratch
	json_in = read_json(annotation_file)
	annotations = []
	for image_id in images:
		annotations.extend(json_in[image_id])
	# Saving the file
	save_json(preprocess_file, annotations)
	return annotations

def read_detections(test_file, detection_file, images):
	# If we already computed this file, we just read it
	preprocess_file = get_preprocess_file(test_file, detection_file)
	if os.path.exists(preprocess_file):
		return read_json(preprocess_file)
	# We generate the file from scratch
	json_in = read_json(detection_file)
	detections = {}
	for image_id in images:
		detections[image_id] = json_in[image_id]	
	# Saving the file
	save_json(preprocess_file, detections)
	return detections

def load_test_and_detections(test_file, annotation_file, detection_file):
	print "Reading files...",
	sys.stdout.flush()
	# Reading test images
	f = open(test_file)
	images = [l.rstrip() for l in f]
	f.close()
	# Reading annotations
	annotations = read_annotations(test_file, annotation_file, images)
	# Reading detections
	detections = read_detections(test_file, detection_file, images)
	print "done!"
	return images, annotations, detections

def get_r(at,results,total):
	return sum([results[i] for i in range(at)])*100/float(total)

def get_value(score_config, key):
	if key in score_config:
		if isinstance(score_config[key], basestring):
			return eval(score_config[key])
		return score_config[key]

def main(num_cores, test_file, score_config):
	# configuration variables
	annotation_file = "./mscoco/tagged_annotations/captions_val2014_with_tags_norm.json"
	detection_file  = "./detectors/detections.json"
	# score params
	score_type      = get_value(score_config, "score_type")
	score_method    = get_value(score_config, "score_method")
	f_aggregation   = get_value(score_config, "f_aggregation")
	use_stemming    = get_value(score_config, "use_stemming")
	detector_filter = get_value(score_config, "detector_filter")
	new_word_filter = get_value(score_config, "new_word_filter")

	# Creating tmp folder
	if not os.path.exists("./tmp"):
		os.makedirs("./tmp")
	
	# loading the test set and detections
	images, annotations, detections = load_test_and_detections(test_file, annotation_file, detection_file)

	# selecting the score to use
	if score_type == ScoreType.mil:
		score = MilScore(detector_filter, use_stemming)
	if score_type == ScoreType.espgame:
		score = EspgameScore(detector_filter, use_stemming, new_word_filter, annotations, score_method, f_aggregation)
	if score_type == ScoreType.conceptnet:
		score = ConceptnetScore(detector_filter, use_stemming, new_word_filter, annotations, score_method, f_aggregation)

	# Dividing experiments in several threads
	total_ann, total_imgs = len(annotations), len(images)
	th, results = [], []
	delta = total_ann//num_cores + (1 if total_ann%num_cores != 0 else 0)
	for i in range(num_cores):
		init, end = delta*i, min([delta*(i+1),total_ann])
		print str(init) + " -> " + str(end) 
		
		m = Manager()
		result = m.list(total_imgs*[0])
		results.append(result)
		p = myThread(score, images, annotations, detections, init, end, result)
		p.start()
		th.append(p)

	# Computing results
	res = total_imgs*[0]
	for i in range(len(th)):
		th[i].join()
		for j in range(len(res)):
			res[j] = res[j] + results[i][j]
	positions = [] # array with the real positions (this is useful to compute the mean and the median using numpy)
	for i in range(len(res)):
		if res[i]>0:
			positions.extend(res[i]*[i+1]) # index '0' means retrieval position '1'

	# Computing performance
	print "-------------------------------- Results"
	print "r@1\t", get_r(1,res,total_ann)
	print "r@5\t", get_r(5,res,total_ann)
	print "r@10\t", get_r(10,res,total_ann)
	print "median\t", np.median(np.array(positions))
	print "mean\t", np.mean(np.array(positions))


if __name__ == '__main__':
	# Getting params	
	parser = argparse.ArgumentParser(prog="image_retrieval", description='Runs an image retrieval experiment using a part of the MSCOCO validation set.')
	parser.add_argument('--num_cores', default=8, type=int, 
						help='It is the number of cores in which you want to split the experimental computation.')
	parser.add_argument('--test_file', default='./tests/coco-1k.txt', type=str, 
						help='It is the path to the file with a list of the images to use in the experiments.')
	parser.add_argument('--score', default='./config/mil.json', type=str, 
						help='It is the path to the file with the score configuration.')
	args = parser.parse_args()
	# Running image retrieval experiment
	num_cores    = args.num_cores
	test_file    = args.test_file
	score_config = read_json(args.score)
	main(num_cores, test_file, score_config)
