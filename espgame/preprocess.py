"""
This code transforms the ESPGAME labels into only one .json file
"""
import os, json, sys
sys.path.insert(0, '../util')
from util import *

def preprocess_espgames():
	print "Preprocessing espgame captions..."
	path = "./ESPGame100k/labels"

	# Create new folder
	folder_out = "./data"
	file_out = os.path.join(folder_out, "espgame.json")
	if not os.path.exists(folder_out):
		os.makedirs(folder_out)

	# Reading the annotations 
	files, total = get_label_paths(path)
	json_out = []
	for i in range(total):
		if i % 1000 == 0: print str(i) + '/' + str(total)
		captions = read_file(files[i])
		json_out.append(captions)
	
	# saving
	save_json(file_out, json_out)

"""
Returns the list of caption files
"""
def get_label_paths(path):
	ret = []
	for f in os.listdir(path):
		if f.endswith(".jpg.desc"):
			ret.append(os.path.join(path, f))
	return ret, len(ret)

"""
returns a set with the captions in a file
"""
def read_file(path):
	f = open(path)
	ret = [l.rstrip() for l in f if is_ascii(l)]
	f.close()
	return list(set(ret))


if __name__ == "__main__":
	preprocess_espgames()